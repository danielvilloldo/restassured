#!groovy

node {
    try {
        stage('Clone repository') {            
            git branch: 'restassured_test', url: 'https://danielvilloldo@bitbucket.org/danielvilloldo/restassured.git'
        }
        stage('Execute tests') {
            script {
                bat 'gradle cleanTest'
                bat 'gradle test'
            }
        }
        currentBuild.result = "SUCCESS"
    } catch (Exception e) {
        currentBuild.result = "FAILURE"
        throw e
    } finally {
        emailext(
                to: 'daniel.villoldo@alten.es',
                replyTo: 'daniel.villoldo@alten.es',
                subject: "Build report from - '${env.JOB_NAME}' ",
                body: 'The status of the build ' + env.JOB_NAME + ' was ' + currentBuild.result);
        if (currentBuild.result == "SUCCESS") {
            withCredentials([string(credentialsId: '94370716-e92d-4a90-9c33-536b9803df2b', variable: 'TOKEN')]) {
                bat 'curl --location --request POST \"https://slack.com/api/chat.postMessage?channel=formacionjenkins&text=The+build+status+was+SUCCESS\" --header \"Authorization: Bearer ' + "${TOKEN}" + '\"'
            }
        } else {
            withCredentials([string(credentialsId: '94370716-e92d-4a90-9c33-536b9803df2b', variable: 'TOKEN')]) {
                bat 'curl --location --request POST \"https://slack.com/api/chat.postMessage?channel=formacionjenkins&text=TThe+build+status+was+FAILURE\" --header \"Authorization: Bearer ' + "${TOKEN}" + '\"'
            }
        }
    }
}