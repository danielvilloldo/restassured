package PageObjects;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

public class DeleteJSON extends PageObject {

    Response response = null;

    public void deletePetData(int ID) {
        RestAssured.baseURI = "https://petstore.swagger.io/v2/pet";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.delete("/" + ID);
    }

    public void verifyDeleteCode() {
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
    }

}
