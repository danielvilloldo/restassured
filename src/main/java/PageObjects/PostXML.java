package PageObjects;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

public class PostXML extends PageObject {

    Response response = null;

    public void postPetData(String requestBody) {
        response = RestAssured.given()
                .contentType(ContentType.XML)
                .body(requestBody)
                .post("https://petstore.swagger.io/v2/pet/");
    }

    public void verifyPostCode() {
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
    }

}
