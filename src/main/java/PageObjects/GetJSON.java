package PageObjects;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

public class GetJSON extends PageObject {

    Response response = null;

    public void getPetData(int id) {
        RestAssured.baseURI = "https://petstore.swagger.io/v2/pet";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.get("/" + id);
    }

    public void verifyGet() {
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
    }

}
