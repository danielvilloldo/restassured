package PageObjects;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;

public class PutXML extends PageObject {

    Response response = null;

    public void putPetData(String requestBody) {
        response = RestAssured.given()
                .contentType(ContentType.XML)
                .body(requestBody)
                .put("https://petstore.swagger.io/v2/pet/");
    }

    public void verifyPutCode() {
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
    }

}
