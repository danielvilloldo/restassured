package PageObjects;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import io.restassured.response.Response;

public class PostJSON extends PageObject {

    Response response = null;

    public void postPetData(String requestBody) {
        response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post("https://petstore.swagger.io/v2/pet/");
    }

    public void verifyPostCode() {
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200);
    }

}
