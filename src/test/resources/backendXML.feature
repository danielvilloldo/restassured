@xml
Feature: Get a pet from Swagger Petstore

  @post
  Scenario: Post a new pet in Swagger Petstore
    Given the Swagger Petstore API
    When try to post a new pet with information in XML format
    Then verify the XML post status code is 200

  @get
  Scenario Outline: Get a pet with the ID from Swagger Petstore
    Given the Swagger Petstore API
    When try to get a pet with ID <ID>, get it
    Then verify the get status code is 200

    Examples:
      | ID  |
      | 444 |

  @update
  Scenario: Update the pet name with the new <name> from Swagger Petstore
    Given a pet in Swagger Petstore try to update the pet with the new information in XML format
    When verify the XML put status code is 200

  @delete
  Scenario Outline: Delete a pet from Swagger Petstore given an <ID>
    Given an ID <ID> for a pet, try to delete it
    When verify the delete status code is 200

    Examples:
      | ID  |
      | 444 |