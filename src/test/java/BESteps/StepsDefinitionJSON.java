package BESteps;

import BESteps.SerenitySteps.SerenityStepsJSON;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepsDefinitionJSON {

    @Steps
    SerenityStepsJSON serenityStepsJSON;

    // POST JSON

    @When("^try to post a new pet with information in JSON format$")
    public void postPet() {
        serenityStepsJSON.postData();
    }

    @Then("^verify the post status code is 200$")
    public void getPostStatusCode() {
        serenityStepsJSON.verifyPost();
    }

    // GET

    @Given("^the Swagger Petstore API$")
    public void getAPI() {
    }

    @When("^try to get a pet with ID (\\d+), get it$")
    public void getPet(int id) {
        serenityStepsJSON.getData( 444);
    }

    @Then("^verify the get status code is 200$")
    public void verifyGetStatusCode() {
        serenityStepsJSON.verifyGet();
    }

    // UPDATE

    @Given("^a pet in Swagger Petstore try to update the pet with the new information$")
    public void putPet() {
        serenityStepsJSON.putData();
    }

    @When("^verify the put status code is 200$")
    public void getPutStatusCode() {
        serenityStepsJSON.verifyPut();
    }
    
    // DELETE

    @Given("^an ID (\\d+) for a pet, try to delete it$")
    public void deletePet(int id) {
        serenityStepsJSON.deleteData(444);
    }

    @When("^verify the delete status code is 200$")
    public void getDeleteStatusCode() {
        serenityStepsJSON.verifyDelete();
    }
        
}