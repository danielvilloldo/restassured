package BESteps.SerenitySteps;

import PageObjects.PostXML;
import PageObjects.PutXML;
import net.thucydides.core.annotations.Step;

public class SerenityStepsXML {

    PostXML postPetXML;
    PutXML putPetXML;

    // POST

    String postBodyRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<Pet>\n" +
            "\t<id>444</id>\n" +
            "\t<Category>\n" +
            "\t\t<id>444</id>\n" +
            "\t\t<name>doggie</name>\n" +
            "\t</Category>\n" +
            "\t<name>Mica</name>\n" +
            "\t<photoUrls>\n" +
            "\t\t<photoUrl>string</photoUrl>\n" +
            "\t</photoUrls>\n" +
            "\t<tags>\n" +
            "\t\t<Tag>\n" +
            "\t\t\t<id>444</id>\n" +
            "\t\t\t<name>string</name>\n" +
            "\t\t</Tag>\n" +
            "\t</tags>\n" +
            "\t<status>available</status>\n" +
            "</Pet>";

    @Step("try to post a new pet with information in XML format")
    public void postData() {
        postPetXML.postPetData(postBodyRequest);
    }

    @Step("verify the XML post status code is 200")
    public void verifyPostXML() {
        postPetXML.verifyPostCode();
    }

    // PUT

    String putBodyRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<Pet>\n" +
            "\t<id>444</id>\n" +
            "\t<Category>\n" +
            "\t\t<id>444</id>\n" +
            "\t\t<name>doggie</name>\n" +
            "\t</Category>\n" +
            "\t<name>MiKa</name>\n" +
            "\t<photoUrls>\n" +
            "\t\t<photoUrl>string</photoUrl>\n" +
            "\t</photoUrls>\n" +
            "\t<tags>\n" +
            "\t\t<Tag>\n" +
            "\t\t\t<id>444</id>\n" +
            "\t\t\t<name>string</name>\n" +
            "\t\t</Tag>\n" +
            "\t</tags>\n" +
            "\t<status>available</status>\n" +
            "</Pet>";

    @Step("Given a pet in Swagger Petstore try to update the pet with the new information in XML format")
    public void putData() {
        putPetXML.putPetData(putBodyRequest);
    }

    @Step("When verify the XML put status code is 200")
    public void verifyPutXML() {
        putPetXML.verifyPutCode();
    }

}
