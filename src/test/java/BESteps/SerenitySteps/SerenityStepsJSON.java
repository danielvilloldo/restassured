package BESteps.SerenitySteps;

import PageObjects.DeleteJSON;
import PageObjects.GetJSON;
import PageObjects.PostJSON;
import PageObjects.PutJSON;
import net.thucydides.core.annotations.Step;

public class SerenityStepsJSON {

    PostJSON postPetJSON;
    GetJSON getPetJSON;
    PutJSON putPetJSON;
    DeleteJSON deletePetJSON;

    // POST

    String postBodyRequest = "{\n" +
            "  \"id\": 444,\n" +
            "  \"category\": {\n" +
            "  \t \"id\": 444,\n" +
            "  \t \"name\": \"string\"\n" +
            "  },\n" +
            "  \"name\": \"Mica\",\n" +
            "  \"photoUrls\": [\n" +
            "  \t\"string\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "  \t{         \n" +
            "  \t\"id\": 444,\n" +
            "  \t\"name\": \"string\"\n" +
            "  \t}        \n" +
            "  ],\n" +
            "  \"status\": \"available\"\n" +
            "}";

    @Step("try to post a new pet with information in JSON format")
    public void postData() {
        postPetJSON.postPetData(postBodyRequest);
    }

    @Step("verify the status code is 200")
    public void verifyPost() {
        postPetJSON.verifyPostCode();
    }

    // GET

    @Step("When try to get a pet with ID \"([^\"]*)\", get it")
    public void getData(int id) {
        getPetJSON.getPetData(444);
    }

    @Step("Then verify the get status code is 200")
    public void verifyGet() {
        getPetJSON.verifyGet();
    }

    // PUT

    String putBodyRequest = "{\n" +
            "  \"id\": 444,\n" +
            "  \"category\": {\n" +
            "  \t \"id\": 444,\n" +
            "  \t \"name\": \"string\"\n" +
            "  },\n" +
            "  \"name\": \"Mika\",\n" +
            "  \"photoUrls\": [\n" +
            "  \t\"string\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "  \t{         \n" +
            "  \t\"id\": 444,\n" +
            "  \t\"name\": \"string\"\n" +
            "  \t}        \n" +
            "  ],\n" +
            "  \"status\": \"available\"\n" +
            "}";

    @Step("Given a pet in Swagger Petstore try to update the pet with the new information")
    public void putData() {
        putPetJSON.putPetData(putBodyRequest);
    }

    @Step("When verify the status code is 200")
    public void verifyPut() {
        putPetJSON.verifyPutCode();
    }

    // DELETE

    @Step("Given an ID (\\d+) for a pet, try to delete it")
    public void deleteData(int ID) {
        deletePetJSON.deletePetData(444);
    }

    @Step("When verify the status code is 200")
    public void verifyDelete() {
        deletePetJSON.verifyDeleteCode();
    }


}
