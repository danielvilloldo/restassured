package BESteps;

import BESteps.SerenitySteps.SerenityStepsXML;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class StepsDefinitionXML {

    @Steps
    SerenityStepsXML serenityStepsXML;

    // POST

    @When("^try to post a new pet with information in XML format$")
    public void postPetXML() {
        serenityStepsXML.postData();
    }

    @Then("^verify the XML post status code is 200$")
    public void getPostStatusCodeXML() {
        serenityStepsXML.verifyPostXML();
    }

    // UPDATE

    @Given("^a pet in Swagger Petstore try to update the pet with the new information in XML format$")
    public void putPetXML() {
        serenityStepsXML.putData();
    }

    @When("^verify the XML put status code is 200$")
    public void getPutStatusCodeXML() {
        serenityStepsXML.verifyPutXML();
    }

}