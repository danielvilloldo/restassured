import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
/*
        features = {"src/test/resources/backendJSON.feature"},
        glue = {"BESteps"},
        tags = {"@json"}
*/

        features = {"src/test/resources/backendXML.feature"},
        glue = {"BESteps"},
        tags = {"@xml"}

)

public class TestRunner {
}
